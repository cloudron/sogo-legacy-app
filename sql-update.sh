#!/bin/bash

set -e

# modified script from https://github.com/inverse-inc/sogo/blob/f66278131267ecc67363e303a8edabf5f2d780fb/Scripts/sql-update-3.2.10_to_3.3.0-mysql.sh

sql="mysql --silent --user=$MYSQL_USERNAME --password=$MYSQL_PASSWORD --host=$MYSQL_HOST --port=$MYSQL_PORT $MYSQL_DATABASE -e "

function growUserProfile() {
    $sql "ALTER TABLE sogo_user_profile MODIFY c_defaults LONGTEXT;"
    $sql "ALTER TABLE sogo_user_profile MODIFY c_settings LONGTEXT;"
}

function growMailInContactsQuick() {
    $sql "ALTER TABLE $table MODIFY c_mail text;"
}

function addCertificateInContactsQuick() {
    # we ignore errors in this case, as the table might already have the field
    $sql "ALTER TABLE $table ADD c_hascertificate INT4 DEFAULT 0;" || true
}

echo "Converting c_content from TEXT to LONGTEXT in the sogo_user_profile table" >&2
growUserProfile

echo "Converting c_mail from VARCHAR(255) to TEXT in Contacts quick tables" >&2
tables=`$sql "select SUBSTRING_INDEX(c_quick_location, '/', -1) from sogo_folder_info where c_path3 = 'Contacts';"`
for table in $tables;
do
    growMailInContactsQuick
    addCertificateInContactsQuick
done

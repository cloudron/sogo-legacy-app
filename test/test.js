#!/usr/bin/env node

'use strict';

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    until = webdriver.until;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var chrome = require('selenium-webdriver/chrome');
    var server, browser = new chrome.Driver();

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    var LOCATION = 'test';
    var EVENT_TITLE = 'Meet the Cloudron Founders';
    var CONTACT_CN = 'Max Mustermann';
    var TEST_TIMEOUT = 200000;
    var app;

    function waitForElement(elem, callback) {
         browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT).then(function () {
                callback();
            });
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    }

    function login(done) {
        browser.manage().deleteAllCookies();
        browser.get('https://' + app.fqdn);

        waitForElement(by.id('input_1'),function () {
            browser.findElement(by.id('input_1')).sendKeys(process.env.USERNAME);
            browser.findElement(by.id('input_2')).sendKeys(process.env.PASSWORD);
            browser.findElement(by.name('loginForm')).submit();
            browser.wait(until.elementLocated(by.xpath('//*[@aria-label="New Event"]')), TEST_TIMEOUT).then(function () { done(); });
        });
    }

    function addContact(done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.USERNAME + '/Contacts/view#/addressbooks/personal/card/new');

        waitForElement(by.xpath('//*[@aria-label="New Contact"]'), function () {
            browser.findElement(by.xpath('//*[@aria-label="New Contact"]')).click();

            // open animation
            browser.sleep(2000).then(function () {

                waitForElement(by.xpath('//*[@aria-label="Create a new address book card"]'), function () {
                    browser.findElement(by.xpath('//*[@aria-label="Create a new address book card"]')).click();

                    waitForElement(by.xpath('//*[@ng-model="editor.card.c_cn"]'), function () {
                        browser.findElement(by.xpath('//*[@ng-model="editor.card.c_cn"]')).sendKeys(CONTACT_CN);
                        browser.findElement(by.xpath('//*[@aria-label="Save"]')).click();

                        waitForElement(by.xpath('//*[text()="' + CONTACT_CN + '"]'), done);
                    });
                });
            });
        });
    }

    function getContact(done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.USERNAME + '/Contacts/view');

        waitForElement(by.xpath('//*[text()="' + CONTACT_CN + '"]'), done);
    }

    function eventExists(done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.USERNAME + '/Calendar/view');

        browser.sleep(2000);

        waitForElement(by.xpath('//*[text()[contains(., "' + EVENT_TITLE + '")]]'), function () {
            browser.findElement(by.xpath('//*[text()[contains(., "' + EVENT_TITLE + '")]]')).click();

            // wait for open
            browser.sleep(2000);

            waitForElement(by.xpath('//*[text()="' + EVENT_TITLE + '"]'), done);
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can login', login);

    it('can create event', function (done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.USERNAME + '/Calendar/view');

        waitForElement(by.xpath('//*[@aria-label="New Event"]'), function () {
            browser.findElement(by.xpath('//*[@aria-label="New Event"]')).click();

            // open animation
            browser.sleep(2000);

            waitForElement(by.xpath('//*[@aria-label="Create a new event"]'), function () {
                browser.findElement(by.xpath('//*[@aria-label="Create a new event"]')).click();

                waitForElement(by.xpath('//*[@ng-model="editor.component.summary"]'), function () {
                    browser.findElement(by.xpath('//*[@ng-model="editor.component.summary"]')).sendKeys(EVENT_TITLE);
                    browser.findElement(by.xpath('//*[@ng-model="editor.component.summary"]')).submit();

                    // wait for save
                    browser.sleep(2000).then(function () { done(); });
                });
            });
        });
    });

    it('event is present', eventExists);
    it('can add contact', addContact);
    it('can get contact', getContact);

    it('can visit mailbox', function (done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.USERNAME + '/Mail/view');

        waitForElement(by.xpath('//*[@aria-label="Write a new message"]'), done);
    });

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('event is still present', eventExists);
    it('can get contact', getContact);

    it('can visit mailbox', function (done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.USERNAME + '/Mail/view');

        waitForElement(by.xpath('//*[@aria-label="Write a new message"]'), done);
    });

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('event is still present', eventExists);
    it('can get contact', getContact);

    it('can visit mailbox', function (done) {
        browser.get('https://' + app.fqdn + '/SOGo/so/' + process.env.USERNAME + '/Mail/view');

        waitForElement(by.xpath('//*[@aria-label="Write a new message"]'), done);
    });

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // update tests
    it('can install app for update', function () {
        execSync('cloudron install --new --wait --appstore-id nu.sogo.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('can add contact', addContact);
    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can get contact', getContact);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});

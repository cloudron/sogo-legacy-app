#!/bin/bash

set -eu -o pipefail

echo
echo "##########################"
echo "##     SOGo startup     ##"
echo "##########################"
echo

if [[ ! -f /app/data/sogo.conf ]]; then
    echo "=> New installation or update from old version"
    echo "=> Create inital sogo.conf"
    cp /app/code/sogo.conf.initial /app/data/sogo.conf
else
    echo "=> Convert the sql schema"
    /app/code/sql-update.sh
fi

echo "=> Patch sogo.conf"
sed -e "s,SOGoProfileURL.*,SOGoProfileURL = \"${MYSQL_URL}/sogo_user_profile\";," \
    -e "s,OCSFolderInfoURL.*,OCSFolderInfoURL = \"${MYSQL_URL}/sogo_folder_info\";," \
    -e "s,OCSSessionsFolderURL.*,OCSSessionsFolderURL = \"${MYSQL_URL}/sogo_sessions_folder\";," \
    -e "s,OCSEMailAlarmsFolderURL.*,OCSEMailAlarmsFolderURL = \"${MYSQL_URL}/sogo_alarms_folder\";," \
    -e "s/baseDN.*/baseDN = \"${LDAP_USERS_BASE_DN}\";/" \
    -e "s/bindDN.*/bindDN = \"${LDAP_BIND_DN}\";/" \
    -e "s/bindPassword.*/bindPassword = \"${LDAP_BIND_PASSWORD}\";/" \
    -e "s,hostname.*,hostname = \"${LDAP_URL}\";," \
    -e "s,SOGoIMAPServer.*,SOGoIMAPServer = \"imaps://${MAIL_IMAP_SERVER}:${MAIL_IMAP_PORT}\";," \
    -e "s/SOGoSMTPServer.*/SOGoSMTPServer = \"${MAIL_SMTP_SERVER}:${MAIL_SMTP_PORT}\";/" \
    -e "s/SOGoMailDomain.*/SOGoMailDomain = \"${MAIL_DOMAIN}\";/" \
    -e "s,SOGoSieveServer.*,SOGoSieveServer = \"sieve://${MAIL_SIEVE_SERVER}:${MAIL_SIEVE_PORT}/?tls=YES\";," \
    -i /app/data/sogo.conf

echo "=> Generating nginx.conf"
sed -e "s,##HOSTNAME##,${APP_DOMAIN}," \
    /app/code/nginx.conf  > /run/nginx.conf

echo "=> Ensure directories"
mkdir -p /run/GNUstep /run/nginx

echo "=> Make cloudron own the data"
mkdir -p /app/data/spool
chown -R cloudron:cloudron /run /app/data

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i SOGo

This app packages SOGo <upstream>4.0.0</upstream>

### Overview

SOGo offers multiple ways to access the calendaring, address book data and is integrated with Cloudron email.
Your users can either use a web browser, Microsoft Outlook, Mozilla Thunderbird, Apple iCal, or a mobile device to access the same information.

### Features

 * Setup for Cloudron email usage
 * CardDav and CalDav synchronisation server
 * Calendar and address book sharing
 * Recurring Events
 * Reminders
 * Free/Busy time
 * Notifications
 * Resource Reservation
 * Categories
 * Material Design Webinterface

### Desktop Clients

SOGo provides perfect integration with desktop clients such as:

 * Mozilla Thunderbird
 * Microsoft Outlook
 * Apple iCal and AddressBook

### Mobile Clients

With SOGo, data access goes beyond the Web and native interfaces. SOGo supports virtually every mobile devices such as:

 * Apple iPhone/iPad and BlackBerry 10 devices are supported natively through the use of the CalDAV and CardDAV protocols, which are well supported by SOGo
 * Windows Phone and Android devices can use 3rdparty CalDAV and CardDAV apps
